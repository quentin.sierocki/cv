
const SQL = "SQL"
const JAVA = "Java"
const JAVASCRIPT = "Javascript"
const HTML = "Html"
const CSSS = "CSS"
const SCSS = "SCSS"
const JSP = "jsp"
const SPRING = "Spring"
const SPRING_BATCH = "Spring Batch"
const SPRING_DATA = "Spring Data"
const SPRING_JMS = "Spring JMS"
const STRUTS = "Struts"
const JENKINS = "Jenkins"
const SONAR = "Sonar"
const KSH = "ksh"
const SH = "sh"
const SVN = "Svn"
const LIQUIBASE = "Liquibase"
const FONCTIONNEL = "Fonctionnel"
const HIBERNATE = "Hibernate"
const JQUERY = "jquery"
const PRIMEFACES = "Primefaces"
const HQL = "HQL"
const TOMCAT = "Tomcat"
const MAVEN = "Maven"
const ORACLE = "Oracle"
const SPRING_AOP = "Spring AOP"
const QUALITY_CENTER = "Quality Center"
const RICHFACES = "RichFaces"
const GIT = "git"
const ELASTICSEARCH = "ElasticSearch"
const KIBANA = "Kibana"
const MANTIS = "Mantis"
const JMETER = "JMETER"
const BACKBONE = "Backbone"
const MARIONETTE = "Marionette"
const ECLIPSE = "eclipse"
const INTELLIJ = "IntelliJ"
const JBOSS = "JBoss"
const KARMA = "karma"
const WEBVIEW = "Webview"
const JIRA = "Jira"
const SPRINGBOOT = "Spring Boot"
const WEBPACK = "webpack"
const ANSIBLE = "Ansible"
const VXML = "Vxml"
const CUCUMBER = "Cucumber"
const GITLAB_CI = "gitlab CI"
const SELENIUM = "Selenium"
const OPENSHIFT = "Openshift"
const SPLUNK = "Splunk"
const CONFLUENCE = "Confluence"
const POSTMAN = "Postman"

const color = {
   BACK: "#f6c453",
   FRONT: "#f0a04b",
   FONCTIONNEL: "#fefbe9",
   DEVOBS: "#e1eedd"
}
const technoColors = {
   "sh":color.BACK,
   "Postman":color.BACK,
   "Confluence":color.FONCTIONNEL,
   "Splunk":color.FONCTIONNEL,
   "Openshift":color.DEVOBS,
   "Selenium": color.BACK,
   "SQL": color.BACK,
   "Java": color.BACK,
   "Javascript": color.FRONT,
   "Html": color.FRONT,
   "CSS": color.FRONT,
   "SCSS": color.FRONT,
   "jsp": color.BACK,
   "Spring": color.BACK,
   "Spring Batch": color.BACK,
   "Spring Data": color.BACK,
   "Spring JMS": color.BACK,
   "Struts": color.BACK,
   "Jenkins": color.DEVOBS,
   "Sonar": color.DEVOBS,
   "ksh": "yellow",
   "Svn": color.DEVOBS,
   "Liquibase": color.BACK,
   "Fonctionnel": color.FONCTIONNEL,
   "Hibernate": color.BACK,
   "jquery": color.FRONT,
   "Primefaces": color.BACK,
   "HQL": color.BACK,
   "Tomcat": color.DEVOBS,
   "Maven": color.BACK,
   "Oracle": color.BACK,
   "Spring AOP": color.BACK,
   "Quality Center": color.FONCTIONNEL,
   "RichFaces": color.BACK,
   "git": color.DEVOBS,
   "ElasticSearch": color.BACK,
   "Kibana": color.BACK,
   "Mantis": color.FONCTIONNEL,
   "JMETER": color.BACK,
   "Backbone": color.FRONT,
   "Marionette": color.FRONT,
   "eclipse": color.BACK,
   "IntelliJ": color.BACK,
   "JBoss": color.DEVOBS,
   "karma": color.FRONT,
   "Webview": color.FRONT,
   "Jira": color.DEVOBS,
   "Spring Boot": color.BACK,
   "webpack": color.FRONT,
   "Ansible": color.DEVOBS,
   "Vxml": color.BACK,
   "Cucumber": color.BACK,
   "gitlab CI": color.DEVOBS
}
const monthNames = ["Janvier", "Fèvrier", "Mars", "Avril", "Mai", "Juin",
   "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"
];

let classColorClient = {
   "SNCF": "sncf-color",
   "Knowmore": "knowmore-color",
   "Société Générale": "societe-generale-color",
   "Orange": "orange-color",
   "LBP": "banque-postale-color"
}

let experiences = [
   {
      employeur: "Sopra Steria",
      dateDebut: new Date(2015, 4, 10),
      dateFin: new Date(2016, 8, 30),
      clients: [{
         client: "SNCF",
         dateDebut: new Date(2015, 4, 10),
         dateFin: new Date(2016, 8, 30),
         missions: [{
            lieu: "Lille",
            dateDebut: new Date(2015, 4, 10),
            dateFin: new Date(2015, 11, 31),
            title: "Maintenance et Développement de DEFRAIL",
            technologies: [SQL, JAVA, JAVASCRIPT, HTML, CSSS, JSP, SPRING, STRUTS, SVN, MAVEN, ORACLE, ECLIPSE],
            contexte: "Dans un centre de service pour la SNCF groupant une quinzaine d'application à maintenir pour une équipe d'une quinzaine de personnes.",
            sujet: "Application \"DEFRAIL\" qui gère l'état des rails sur l'ensemble du réseau SNCF.",
            poste: "Ingénieur JAVA/JEE",
            taches: [
               {
                  technologies: [JAVA, SPRING, JAVASCRIPT, HTML, JSP, CSSS, JENKINS, SONAR, SVN, MAVEN],
                  tache: "Développement d'une nouvelle fonctionnalité de documentation."
               },
               {
                  technologies: [JAVA, SPRING],
                  tache: "Migration de l'application vers Spring."
               },
               {
                  technologies: [SQL],
                  tache: "Optimisation de requête SQL."
               },
               {
                  technologies: [SQL],
                  tache: "Livraison en recette."
               },
               {
                  technologies: [KSH],
                  tache: "Exécution de batch ksh."
               }
            ],
            resultats: [
               "Montée en compétence sur les technologies WEB", "Respect des délais, testés et livrés.", "Augmentation des performances et de la qualité (Sonar)"
            ]
         },
         {
            lieu: "Paris",
            dateDebut: new Date(2016, 0, 1),
            dateFin: new Date(2016, 8, 31),
            title: "Développement de NOPANIC",
            technologies: [ECLIPSE, SQL, HQL, HIBERNATE, SPRING_DATA, SPRING_JMS, SPRING_BATCH, SPRING_AOP, SONAR, JENKINS, TOMCAT, JAVA, JAVASCRIPT, JQUERY, HTML, CSSS, JSP, SPRING, ORACLE, SVN],
            contexte: "Suite à un problème de charge sur leur ancienne application qui gère l'état des incidents de train la SNCF recrée from scratch une nouvelle application plus performante.",
            sujet: "Application \"NOPANIC\" qui gére en temps réel les incidents de ligne sur l'ensemble du traffic SNCF.",
            poste: "Ingénieur JAVA/JEE",
            taches: [
               {
                  technologies: [FONCTIONNEL],
                  tache: "Recueil et analyse des besoins."
               },
               {
                  technologies: [FONCTIONNEL, JAVA],
                  tache: "Conception technique"
               },
               {
                  technologies: [JAVA, PRIMEFACES, HTML, CSSS, JAVASCRIPT, JQUERY],
                  tache: "Utilisation du Framework JSF/primefaces (couche présentation)"
               },
               {
                  technologies: [TOMCAT, JENKINS],
                  tache: "Déploiement sur le serveur d’application tomcat 6"
               },
               {
                  technologies: [SVN, MAVEN, SONAR],
                  tache: "Intégration continue : Gestion des sources et de leur historique par l’outil SVN."
               },
               {
                  technologies: [JAVA],
                  tache: "Mise en polace et réalisation des tests unitaires"
               },
               {
                  technologies: [JAVA, SPRING, SPRING_BATCH],
                  tache: "Réalisation des batchs Java de mise à jour des circulations."
               },
               {
                  technologies: [JAVA, SPRING],
                  tache: "Utilisation de Spring core pour gérer l'injection de dépendances."
               },
               {
                  technologies: [JAVA, SPRING, SPRING_JMS],
                  tache: "Utilisation de Spring JMS pour gérer les messages JMS (écouter les messages JMS (Listener) issues du MQ puis les consommer)"
               },
               {
                  technologies: [JAVA, SPRING, SPRING_AOP],
                  tache: "Utilisation de Spring AOP pour gérer l'ensemble des alertes par mail, sms ou infoglobale."
               },
               {
                  technologies: [JAVA, HIBERNATE, SPRING_DATA],
                  tache: "Utilisation d'hibernate pour l'intéraction avec la BDD."
               },
               {
                  technologies: [MAVEN, JENKINS],
                  tache: "Packaging et déploiement de l’EAR dans un environnement d’intégration"
               },
               {
                  technologies: [JAVA],
                  tache: "Réalisation des tests d’intégrations"
               },
               {
                  technologies: [SQL, HQL, SONAR, JENKINS, TOMCAT, JAVA, JAVASCRIPT, JQUERY, HTML, CSSS, JSP, SPRING, ORACLE],
                  tache: "Corrections des anomalies"
               },
               {
                  technologies: [FONCTIONNEL, QUALITY_CENTER],
                  tache: "Correction des bugs : utilisation de Quality Center pour le workflow des anomalies"
               },
               {
                  technologies: [FONCTIONNEL, MAVEN],
                  tache: "Création des livrables et suivi des livraisons sur l’environnement de production"
               }
            ],
            resultats: [
               "Application mise en production.", "Satisfaction de la part du client.", "Augmentation des performances et de la qualité (Sonar)", "Nette amélioration des performances suite à un chantier performance validé par le client."
            ]
         }]
      }]
   }, {
      employeur: "Alten",
      dateDebut: new Date(2016, 9, 31),
      dateFin: new Date(),
      clients: [
         {
            client: "Knowmore",
            dateDebut: new Date(2016, 9, 1),
            dateFin: new Date(2017, 5, 30),
            missions: [
               {
                  lieu: "Paris",
                  title: "Développement sur SK2",
                  dateDebut: new Date(2016, 9, 1),
                  dateFin: new Date(2017, 5, 30),
                  technologies: [ECLIPSE, SQL, JAVA, JAVASCRIPT, HTML, CSSS, JSP, SPRING, STRUTS, GIT, MAVEN, ORACLE, ELASTICSEARCH, RICHFACES, JQUERY, JMETER],
                  contexte: "Une application reprise par knowmore pour gérer le versionnement des documents au sein d'un entreprise.",
                  sujet: "Application qui gére en temps réel les versions de documents au sein de grandes entreprises(Safran, Orange...).",
                  poste: "Ingénieur JAVA/JEE",
                  taches: [
                     {
                        technologies: [FONCTIONNEL],
                        tache: "Recueil et analyse des besoins."
                     },
                     {
                        technologies: [FONCTIONNEL, JAVA],
                        tache: "Conception technique"
                     },
                     {
                        technologies: [JAVA, RICHFACES, HTML, CSSS, JAVASCRIPT, JQUERY],
                        tache: "Utilisation du Framework JSF/richfaces (couche présentation)"
                     },
                     {
                        technologies: [TOMCAT, JENKINS],
                        tache: "Déploiement sur le serveur d’application tomcat 6"
                     },
                     {
                        technologies: [GIT, MAVEN, SONAR],
                        tache: "Intégration continue : Gestion des sources et de leur historique par l’outil git."
                     },
                     {
                        technologies: [JAVA, ELASTICSEARCH],
                        tache: "Réalisation de batchs java de mise à jour des dates, d’envois de mails et de mise à jour ElasticSearch."
                     },
                     {
                        technologies: [ELASTICSEARCH],
                        tache: "Refactoring intégral de la partie ElasticSearch pour cause de lenteur.(gros problème chez Orange notament)."
                     },
                     {
                        technologies: [SQL, HIBERNATE, ELASTICSEARCH, JMETER],
                        tache: "Enquête sur un blocage de l’application."
                     },
                     {
                        technologies: [MAVEN, GIT],
                        tache: "Packaging et déploiement de l’EAR dans un environnement d’intégration"
                     },
                     {
                        technologies: [SQL, JAVA, JAVASCRIPT, HTML, CSSS, JSP, SPRING, STRUTS, GIT, MAVEN, ORACLE, ELASTICSEARCH, RICHFACES, JQUERY],
                        tache: "Corrections des anomalies"
                     },
                     {
                        technologies: [FONCTIONNEL, MANTIS],
                        tache: "Correction des bugs : utilisation de Quality Center pour le workflow des anomalies"
                     },
                     {
                        technologies: [FONCTIONNEL, MAVEN],
                        tache: "Création des livrables et suivi des livraisons sur l’environnement de production"
                     }
                  ],
                  resultats: [
                     "Performance ElasticSearch fortement augmentée(facteur 10)", "Problème de blocage + Plan d’actions effectué + Nombreuses sources d’informations supplémentaires pour identifier le blocage.", "Satisfaction client et proposition d'embauche à la fin."
                  ]
               }]
         },
         {
            client: "Société Générale",
            dateDebut: new Date(2017, 6, 1),
            dateFin: new Date(2019, 9, 31),
            missions: [
               {

                  lieu: "Paris",
                  dateDebut: new Date(2016, 9, 1),
                  dateFin: new Date(2018, 0, 1),
                  title: "Développement de GCD",
                  technologies: [JBOSS, INTELLIJ, BACKBONE, MARIONETTE, JAVA, JAVASCRIPT, HTML, SCSS, GIT, MAVEN, JQUERY, JIRA, QUALITY_CENTER],
                  contexte: "Une application multiplateforme de Gestion Conseillé Digitale au sein de la Société Générale dans le cadre de sa migration vers le tout digital.",
                  sujet: "Application qui restitue les actions effectués par les conseillers financiers de la banque à des clients Banque privée et permet d'effectuer un arbitrage.",
                  poste: "Développeur FullStack",
                  taches: [
                     {
                        technologies: [FONCTIONNEL, JAVA],
                        tache: "Conception technique"
                     },
                     {
                        technologies: [BACKBONE, MARIONETTE,INTELLIJ],
                        tache: "Utilisation du Framework JSF/richfaces (couche présentation)"
                     },
                     {
                        technologies: [JBOSS],
                        tache: "Déploiement sur le serveur d’application JBoss10.0"
                     },
                     {
                        technologies: [JAVA],
                        tache: "Utilisation de Web-services REST et SOAP pour interagir avec la BDD."
                     },
                     {
                        technologies: [MAVEN, GIT],
                        tache: "Gestion des sources et de leur historique par l’outil Git."
                     },
                     {
                        technologies: [KARMA, JAVA],
                        tache: "Mise en place et Réalisation des tests unitaires"
                     },
                     {
                        technologies: [JAVASCRIPT],
                        tache: "Mise en place de traces applicatifs, de traces BigDatas et de traces d’analyse de comportement client."
                     },
                     {
                        technologies: [FONCTIONNEL, JAVA, JAVASCRIPT],
                        tache: "Sécurité Informatique"
                     },
                     {
                        technologies: [JAVASCRIPT, WEBVIEW],
                        tache: "Application mobile - site responsive"
                     },
                     {
                        technologies: [FONCTIONNEL, JIRA, QUALITY_CENTER],
                        tache: "Suivi des livraisons sur l’environnement de production"
                     },

                  ],
                  resultats: [
                     "Grande variété de tache effectuée", "Sécurité informatique (scellement, cryptage)", "Développement mobile via intégration dans une webview"
                  ]
               },
               {
                  lieu: "Paris",
                  title: "Développement de Wealth Aggregation",
                  dateDebut: new Date(2018, 0, 1),
                  dateFin: new Date(2019, 9, 31),
                  technologies: [JBOSS, INTELLIJ, BACKBONE, MARIONETTE, JAVA, JAVASCRIPT, HTML, SCSS, GIT, MAVEN, JQUERY, JIRA, QUALITY_CENTER],
                  contexte: "Une application multiplateforme (IOS, Android, tablette) d’aggrégation d’actifs.",
                  sujet: "Application qui restitue l’ensemble des portefeuilles d’un client(toute banque) sur une seule page afin de le faire réfléchir sur l'orientation de ces investissements.",
                  poste: "Développeur FullStack",
                  taches: [
                     {
                        technologies: [FONCTIONNEL, JAVA],
                        tache: "Conception technique"
                     },
                     {
                        technologies: [BACKBONE, MARIONETTE,INTELLIJ],
                        tache: "Utilisation du Framework JSF/richfaces (couche présentation)"
                     },
                     {
                        technologies: [JBOSS],
                        tache: "Déploiement sur le serveur d’application JBoss10.0"
                     },
                     {
                        technologies: [JAVA],
                        tache: "Utilisation de Web-services REST et SOAP pour interagir avec la BDD."
                     },
                     {
                        technologies: [MAVEN, GIT],
                        tache: "Gestion des sources et de leur historique par l’outil Git."
                     },
                     {
                        technologies: [KARMA, JAVA],
                        tache: "Mise en place et Réalisation des tests unitaires"
                     },
                     {
                        technologies: [JAVASCRIPT],
                        tache: "Mise en place de traces applicatifs, de traces BigDatas et de traces d’analyse de comportement client."
                     },
                     {
                        technologies: [FONCTIONNEL, JAVA, JAVASCRIPT],
                        tache: "Sécurité Informatique"
                     },
                     {
                        technologies: [JAVASCRIPT, WEBVIEW],
                        tache: "Application mobile - site responsive"
                     },
                     {
                        technologies: [FONCTIONNEL, JIRA, QUALITY_CENTER],
                        tache: "Suivi des livraisons sur l’environnement de production"
                     },

                  ],
                  resultats: [
                     "Grande variété de tache effectuée", "Sécurité informatique (scellement, cryptage)", "Développement mobile via intégration dans une webview"
                  ]
               }
            ]
         },
         {
            client: "Orange",
            dateDebut: new Date(2019, 10, 1),
            dateFin: new Date(2022, 1, 31),
            missions: [
               {
                  lieu: "Bordeaux",
                  title: "Développement de l'IHM PILA",
                  dateDebut: new Date(2019, 10, 1),
                  dateFin: new Date(2020, 6, 30),
                  technologies: [HIBERNATE, LIQUIBASE, GITLAB_CI, VXML, SPRINGBOOT, INTELLIJ, TOMCAT, WEBPACK, JSP, CUCUMBER, JAVA, JAVASCRIPT, HTML, SCSS, GIT, MAVEN, JQUERY, JIRA, ANSIBLE],
                  contexte: "Pila est une application avec une grosse dette technique, une refonte de l'IHM est envisagée afin de la diminuer.",
                  sujet: "Développement from scratch d'une nouvelle IHM.",
                  poste: "Développeur FullStack",
                  taches: [
                     {
                        technologies: [INTELLIJ,FONCTIONNEL, WEBPACK, GITLAB_CI, JAVA, JAVASCRIPT, MAVEN, GIT, ANSIBLE, LIQUIBASE],
                        tache: "Mise en place de l'architecture IHM."
                     },
                     {
                        technologies: [JAVASCRIPT, HTML, SCSS, JSP],
                        tache: "Développement de vingts pages d'IHM."
                     },
                     {
                        technologies: [CUCUMBER, SPRINGBOOT, LIQUIBASE, SELENIUM],
                        tache: "test Intégration"
                     }
                  ],
                  resultats: [
                     "Grande variété de tache effectuée", "Montée en compétence sur Webpack,Spring-Boot et gitlab-ci", "Satisfaction Client"
                  ]
               },
               {
                  lieu: "Bordeaux",
                  title: "Développement de Robyn",
                  dateDebut: new Date(2020, 7, 1),
                  dateFin: new Date(2022, 1, 31),
                  technologies: [HIBERNATE, LIQUIBASE, GITLAB_CI, VXML, SPRINGBOOT, INTELLIJ, TOMCAT, WEBPACK, JSP, CUCUMBER, JAVA, JAVASCRIPT, HTML, SCSS, GIT, MAVEN, JQUERY, JIRA, ANSIBLE],
                  contexte: "Dans le but de diminuer les coûts, Orange tente d'automatiser la gestion des appels téléphoniques.",
                  sujet: "Développement d'un Middleware IA / voix(Robyn) et refonte d'une ancienne IHM (PILA)",
                  poste: "Développeur FullStack",
                  taches: [
                     {
                        technologies: [INTELLIJ,FONCTIONNEL, WEBPACK, JAVA, JAVASCRIPT, MAVEN, GIT, ANSIBLE],
                        tache: "Mise en place de l'architecture du middleware."
                     },
                     {
                        technologies: [JAVASCRIPT, HTML, SCSS, JSP],
                        tache: "Développement d'une IHM"
                     },
                     {
                        technologies: [CUCUMBER, SPRINGBOOT, LIQUIBASE, SELENIUM],
                        tache: "test Intégration"
                     },
                     {
                        technologies: [SPRINGBOOT, LIQUIBASE, JAVA, VXML],
                        tache: "développement serveur VXML middleware"
                     }
                  ],
                  resultats: [
                     "Découverte du VXML et des serveurs vocaux.", "Montée en compétence sur Springboot,Webpack, Gitlab-ci et Ansible", "Bonne prise en main de Spring-Security"
                  ]
               }
            ]
         },
         {
            client: "LBP",
            dateDebut: new Date(2022, 2, 1),
            dateFin: new Date(2022, 12, 31),
            missions: [
               {
                  lieu: "Bordeaux",
                  title: "Développement d'un Broker d'envoi de notifications",
                  dateDebut: new Date(2022, 2, 1),
                  dateFin: new Date(2022, 12, 31),
                  technologies: [HIBERNATE, SPLUNK, GITLAB_CI, OPENSHIFT, SPRINGBOOT, ECLIPSE, TOMCAT, JBOSS, CUCUMBER, JAVA, JAVASCRIPT, HTML,  GIT, MAVEN, JQUERY, JIRA,CONFLUENCE],
                  contexte: "Application d'envoi de notification transverse à la banque.",
                  sujet: "Développement from scratch du Broker : trop grosse dette technique à mon arrivée",
                  poste: "Développeur FullStack",
                  taches: [
                     {
                        technologies: [ WEBPACK, GITLAB_CI, JAVA, JAVASCRIPT, MAVEN, GIT, OPENSHIFT,POSTMAN],
                        tache: "Mise en place de bonnes pratiques(BDD/TAA/architecture hexagonal/MR)"
                     },
                     {
                        technologies: [JAVASCRIPT, HTML, CSSS, OPENSHIFT],
                        tache: "Développement d'IHM de gestions et aides au développement"
                     },
                     {
                        technologies: [JAVA, SPRINGBOOT],
                        tache: "Mise en place de log avec alerting via Splunk"
                     }
                  ],
                  resultats: [
                     "Refonte et mise en production de l'application", "Gestion intégrale du cycle de vie de l'application", "Satisfaction Client"
                  ]
               }
            ]
         }
      ]
   },
   {
      employeur: "Freelance",
      dateDebut: new Date(2023, 0, 1),
      dateFin: new Date(),
      clients: [
         {
            client: "LBP",
            dateDebut: new Date(2023, 0, 1),
            dateFin: new Date(),
            missions: [
               {
                  lieu: "Bordeaux",
                  title: "Développement d'un Broker d'envoi de notifications",
                  dateDebut: new Date(2023, 0, 1),
                  dateFin: new Date(),
                  technologies: [HIBERNATE, SPLUNK, GITLAB_CI, OPENSHIFT, SPRINGBOOT, ECLIPSE, TOMCAT, JBOSS, CUCUMBER, JAVA, JAVASCRIPT, HTML,  GIT, MAVEN, JQUERY, JIRA,CONFLUENCE],
                  contexte: "Application d'envoi de notification transverse à la banque.",
                  sujet: "Développement du broker et mise en place de nouvelles fonctionnalités",
                  poste: "Développeur FullStack",
                  taches: [
                     {
                        technologies: [ SPRINGBOOT, GITLAB_CI, JAVA, JAVASCRIPT, MAVEN, GIT, OPENSHIFT,SH],
                        tache: "Développement de batch de gestions"
                     },
                     {
                        technologies: [FONCTIONNEL],
                        tache: "Politique pour obtenir des informations / ouvrir des flux"
                     },
                     {
                        technologies: [CONFLUENCE, SPRINGBOOT,GITLAB_CI],
                        tache: "Ecriture de documentation pour les nouveaux arrivants"
                     }
                  ],
                  resultats: [
                     "Refonte et mise en production de l'application", "Gestion intégrale du cycle de vie de l'application", "Satisfaction Client"
                  ]
               }
            ]
         }
      ]
   }
]


function updateDetailsByTechnologie(d3Val) {
   document.querySelector("#details_id").innerHTML = createDetailsByTechnologie(d3Val.name)
}

/**fonctions utiles */

function createDetailsByTechnologie(techno) {
   let html = "<div>"
   html += "<div>"
   html += "<h1 class=\"text-center\">" + techno + "</h1>"
   for (let experience of experiences) {
      for (let client of experience.clients) {
         let needToBeAdded = false
         let htmlToAdd = "<h4>" + client.client + "</h4>"
         htmlToAdd += "<hr>"
         for (let mission of client.missions) {
            let dateDebut = monthNames[mission.dateDebut.getMonth()] + " " + mission.dateDebut.getFullYear()
            let dateFin = monthNames[mission.dateFin.getMonth()] + " " + mission.dateFin.getFullYear()
            htmlToAdd += "<p><i class=\"far fa-clock\"></i>  " + dateDebut + " -> " + dateFin + "</p>"

            for (let tache of mission.taches) {
               if (tache.technologies.indexOf(techno) > -1) {
                  needToBeAdded = true
                  htmlToAdd += "<li>" + tache.tache + "</li>"
               }
            }
            htmlToAdd += "</ul>"
            htmlToAdd += "<hr>"

         }
         if (needToBeAdded) html += htmlToAdd
      }
   }
   html += "</div>"
   html += "</div>"
   return html
}

function createDetailsByYear(year) {
   for (let experience of experiences) {
      for (let client of experience.clients) {
         if (year <= client.dateFin.getFullYear() && year >= client.dateDebut.getFullYear()) {
            return createDetailsByClient(client.client)
         }
      }
   }
   return ""
}

function createDetailsByClient(clientName) {
   let html = "<div>"
   for (let experience of experiences) {
      for (let client of experience.clients) {
         if (client.client == clientName) {
            html += "<h1 class=\"text-center\">" + client.client + "</h1>"

            for (let mission of client.missions) {
               let dateDebut = monthNames[mission.dateDebut.getMonth()] + " " + mission.dateDebut.getFullYear()
               let dateFin = monthNames[mission.dateFin.getMonth()] + " " + mission.dateFin.getFullYear()
               html += "<hr><div>"
               html += "<h3 class=\"text-center\">" + mission.title + "</h3>"
               html += "<p><i class=\"far fa-clock\"></i>  " + dateDebut + " -> " + dateFin + "</p>"
               html += "<h4>Compétences </h4>"
               for (let technologie of mission.technologies) {
                  html += "<span class=\"mr-5 mb-3 badge bg-secondary\">" + technologie + "</span>"
               }
               html += "<h4>Contexte</h4>"
               html += "<p>" + mission.contexte + "</p>"
               html += "<h4>Sujet</h4>"
               html += "<p>" + mission.sujet + "</p>"
               html += "</div>"
               html += "<div>"
               html += "<h4>Taches </h4><ul>"
               for (let tache of mission.taches) {
                  html += "<li>" + tache.tache + "</li>"
               }
               html += "</ul>"
               html += "<h4>Résultats </h4><ul>"
               for (let resultat of mission.resultats) {
                  html += "<li>" + resultat + "</li>"
               }
               html += "</ul>"
               
               html += "</div>"
            }
         }
      }
   }
   html += "</div>"
   return html
}
function _normalizeData() {

   for (let exp of experiences) {
      for (let client of exp.clients) {
         for (let mission of client.missions) {
            let yearDebutMission = mission.dateDebut.getFullYear()
            let yearFinMission = mission.dateFin.getFullYear()
            if (year >= yearDebutMission || year <= yearFinMission) {
               let value = 0;
               if (year == yearDebutMission && year == yearFinMission) {
                  value = (mission.dateFin.getTime() - mission.dateDebut.getTime()) / TIME_IN_ONE_YEAR
               } else if (year == yearDebutMission) {
                  value = (new Date(year + 1, 0, 0, 0, 0).getTime() - mission.dateDebut.getTime()) / TIME_IN_ONE_YEAR
               } else if (year == yearFinMission) {
                  value = (mission.dateFin.getTime() - new Date(year, 0, 0, 0, 0).getTime()) / TIME_IN_ONE_YEAR
               } else if (year > yearDebutMission && year < yearFinMission) {
                  value = 1
               }
               if (value !== 0) {
                  for (let competence of mission.technologies) {
                     let competencePresent = false
                     for (let competenceAlreadyWorked of dataCompetences) {
                        if (competenceAlreadyWorked.name == competence) {
                           competencePresent = true
                           competenceAlreadyWorked.lastValue = competenceAlreadyWorked.value
                           competenceAlreadyWorked.value = Math.round((competenceAlreadyWorked.value + value) * 100) / 100
                        }
                     }
                     if (!competencePresent) {
                        dataCompetences.push({
                           name: competence,
                           colour: technoColors[competence],
                           value: Math.round((value) * 100) / 100,
                           lastValue: 0
                        });
                     }

                  }
               }
            }
         }
      }

   }
   return dataCompetences.sort((a, b) => b.value - a.value).slice(0, top_n);
}


function getMinDate() {
   let dateRetour
   for (let exp of experiences) {
      if (!dateRetour || exp.dateDebut.getTime() < dateRetour.getTime()) {
         dateRetour = exp.dateDebut
      }
   }
   return dateRetour;
}


function prepareDataProgressBar() {
   const timeStart = dateBegin.getTime()
   const timeEnd = dateEnd.getTime()
   const total = timeEnd - timeStart
   let retour = []
   for (let experience of experiences) {
      for (let client of experience.clients) {
         let data = {}
         let beginClient = client.dateDebut.getTime()
         let endClient = client.dateFin.getTime()
         let timeClient = endClient - beginClient
         data.pourcentage = timeClient / total * 100
         data.clientColor = classColorClient[client.client]
         data.dateDebut = client.dateDebut
         data.dateFin = client.dateFin
         data.client = client.client
         retour.push(data)
      }
   }

   return retour;
}
function createSelectYear(year) {
   let html = "<label  class=\"form-label\">Année</label><select id=\"select-year-select\"  class=\"form-select\">"
   for (let i = yearStart; i <= yearEnd; i++) {
      html += "<option " + (year === i ? "selected" : "") + " value = \"" + i + "\" > " + i + "</option >"
   }
   html += "</select>"
   return html
}

function createProgressBarByDate(year) {
   let html = "<div>"
   let progressBarHidden = "<div class=\"progress progress-bar-invisible\">"
   let progressBar = "<div class=\"progress\">"
   for (let data of dataProgressBar) {
      let isActive = year >= data.dateDebut.getFullYear() && year <= data.dateFin.getFullYear()
      let dateDebut = (window.screen.width > 1200 ? monthNames[data.dateDebut.getMonth()] : data.dateDebut.getMonth() + 1) + " " + data.dateDebut.getFullYear()
      let dateFin = (window.screen.width > 1200 ? monthNames[data.dateFin.getMonth()] : data.dateFin.getMonth() + 1) + " " + data.dateFin.getFullYear()
      progressBarHidden += "<div class=\"progress-bar\" role=\"progressbar\" style=\"width: " + data.pourcentage + "%\" aria-valuenow=\"" + data.pourcentage +
         "\" aria-valuemin=\"0\" aria-valuemax=\"100\"><div class=\"d-flex justify-content-around\"><span> " + dateDebut + "</span >" +
         (isActive ? "<span><i class=\"fa-2x fas fa-hiking\" ></i></span>" : "") + "<span>" + dateFin + "</span></div ></div> "
      progressBar += "<div class=\"progress-bar " + (isActive ? "progress-bar-active" : "") + " progress-bar-clickable " + data.clientColor +
         "\" role=\"progressbar\" style=\"width: " + data.pourcentage + "%\" aria-valuenow=\"" + data.pourcentage +
         "\" aria-valuemin=\"0\" aria-valuemax=\"100\"><div class=\"d-flex justify-content-around\">" + data.client + "</span>" +
         (isActive ? "<span class=\"top-down-animation\"><i class=\"fas fa-hand-pointer\"></i></span>" : "") + "</div></div>"
   }
   progressBarHidden += "</div>"
   progressBar += "</div>"
   html += progressBarHidden
   html += progressBar
   html += "<div>"
   return html
}
function tickerFunction(yearSlice) {

   document.querySelector("#select-year-select").value = year
   //Génération de la barre de progression
   document.querySelector("#progress-bar-client").innerHTML = createProgressBarByDate(year)
   var titleExperienceGraph = document.querySelectorAll('.progress-bar-clickable');
   Array.from(titleExperienceGraph).forEach(link => {
      link.addEventListener('click', function (event) {
         document.querySelector("#details_id").innerHTML = createDetailsByClient(event.target.innerText)
      });
   });

   //Création de la zone de détails 
   document.querySelector("#details_id").innerHTML = createDetailsByYear(year)



   x.domain([0, d3.max(yearSlice, d => d.value) + 1]);

   //Adaptation de l'axe des abscisses aux nouvelles valeurs.
   svg.select('.xAxis')
      .transition()
      .duration(tickDuration)
      .ease(d3.easeLinear)
      .call(xAxis);

   const bars = svg.selectAll('.bar').data(yearSlice, d => d.name);

   bars
      .enter()
      .append('rect')
      .attr('class', d => `bar ${d.name.replace(/\s/g, '_')}`)
      .attr('x', x(0) + 1)
      .attr('width', d => x(d.value) - x(0))
      .attr('y', d => y(top_n + 1) + 5)
      .attr('height', y(1) - y(0) - barPadding)
      .style('fill', d => d.colour)
      .transition()
      .duration(tickDuration)
      .ease(d3.easeLinear)
      .attr('y', d => y(d.rank) + 5);

   bars
      .transition()
      .duration(tickDuration)
      .ease(d3.easeLinear)
      .attr('width', d => Math.max(0, x(d.value) - x(0)))
      .attr('y', d => y(d.rank) + 5);

   bars
      .exit()
      .transition()
      .duration(tickDuration)
      .ease(d3.easeLinear)
      .attr('width', d => Math.max(0, x(d.value) - x(0)))
      .attr('y', d => y(top_n + 1) + 5)
      .remove();

   const labels = svg.selectAll('.label')
      .data(yearSlice, d => d.name);


   //Gestion d'ajout des noms dans les graphiques.
   labels
      .enter()
      .append('text')
      .attr('class', 'label')
      .attr('x', d => x(d.value) - 8)
      .attr('y', d => y(top_n + 1) + 5 + ((y(1) - y(0)) / 2))
      .style('text-anchor', 'end')
      .html(d => d.name)
      .transition()
      .duration(tickDuration)
      .ease(d3.easeLinear)
      .attr('y', d => y(d.rank) + 5 + ((y(1) - y(0)) / 2) + 1);



   labels
      .transition()
      .duration(tickDuration)
      .ease(d3.easeLinear)
      .attr('x', d => x(d.value) - 8)
      .attr('y', d => y(d.rank) + 5 + ((y(1) - y(0)) / 2) + 1);

   labels
      .exit()
      .transition()
      .duration(tickDuration)
      .ease(d3.easeLinear)
      .attr('x', d => x(d.value) - 8)
      .attr('y', d => y(top_n + 1) + 5)
      .remove()
      ;


   //ajout des nombres à côté des labels.
   const valueLabels = svg.selectAll('.valueLabel').data(yearSlice, d => d.name);

   valueLabels
      .enter()
      .append('text')
      .attr('class', 'valueLabel')
      .attr('x', d => x(d.value) + 5)
      .attr('y', d => y(top_n + 1) + 5)
      .text(d => d.value)
      .transition()
      .duration(tickDuration)
      .ease(d3.easeLinear)
      .attr('y', d => y(d.rank) + 5 + ((y(1) - y(0)) / 2) + 1);

   valueLabels
      .transition()
      .duration(tickDuration)
      .ease(d3.easeLinear)
      .attr('x', d => x(d.value) + 5)
      .attr('y', d => y(d.rank) + 5 + ((y(1) - y(0)) / 2) + 1)
      .tween("text", function (d) {
         const i = d3.interpolateNumber(d.lastValue, d.value);
         //return i(interpolator);
         return function (t) {
            let v = i(t);
            if (v < 0)
               v = 0;
            this.textContent = v.toFixed(1);
         };
      });


   valueLabels
      .exit()
      .transition()
      .duration(tickDuration)
      .ease(d3.easeLinear)
      .attr('x', d => x(d.value) + 5)
      .attr('y', d => y(top_n + 1) + 5)
      .remove();

   d3.selectAll(".label").on("click", updateDetailsByTechnologie).attr("cursor", "pointer")
   yearText.html(~~year);
}

/* ***************************** Partie graphique        **********************************************    */
const TIME_IN_ONE_YEAR = 31536000000
const top_n = 40;
const height = 800;
const width = window.screen.width < 500 ? window.screen.width : 800;

const tickDuration = 1000; //delay of an animation
const delayDuration = 2000; //delay between two years

const dateBegin = getMinDate()
const yearStart = dateBegin.getFullYear()
const dateEnd = new Date()
const yearEnd = dateEnd.getFullYear();

const title = `Compétences  (${yearStart}-${yearEnd})`;
const subTitle = " en nombre d'années pratiquées";

let dataCompetences = []
let valuesByDate = {}
let year = yearStart;
let lastValues = {};
let yearSlice = {}



// Feel free to change or delete any of the code you see in this editor!
const svg = d3.select("#skills_id").append("svg")
   .attr("width", width)
   .attr("height", height);

const margin = {
   top: 80,
   right: 0,
   bottom: 5,
   left: 0
};

const barPadding = (height - (margin.bottom + margin.top)) / (top_n * 5);

svg.append('text')
   .attr('class', 'title')
   .attr('y', 24)
   .html(title);

svg.append("text")
   .attr("class", "subTitle")
   .attr("y", 55)
   .html(subTitle);

svg.append('text')
   .attr('class', 'caption')
   .attr('x', width)
   .attr('y', height - 5)
   .style('text-anchor', 'end');





let x = d3.scaleLinear()
   .domain([0, 1])
   .range([margin.left, width - margin.right - 65]);

let y = d3.scaleLinear()
   .domain([top_n, 0])
   .range([height - margin.bottom, margin.top]);

let xAxis = d3.axisTop()
   .scale(x)
   .ticks(width > 500 ? 5 : 2)
   .tickSize(-(height - margin.top - margin.bottom))
   .tickFormat(d => d3.format(',')(d));

svg.append('g')
   .attr('class', 'axis xAxis')
   .attr('transform', `translate(0, ${margin.top})`)
   .call(xAxis)
   .selectAll('.tick line')
   .classed('origin', d => d == 0);



let yearText = svg.append('text')
   .attr('class', 'yearText')
   .attr('x', width - margin.right - 200)
   .attr('y', 50)
   .style('text-anchor', 'end')
   .html(~~year);


const dataProgressBar = prepareDataProgressBar()



let ticker = d3.interval(e => {

   //Préparation des données pour le graphique.
   yearSlice = _normalizeData(year);
   yearSlice.forEach((d, i) => d.rank = i);

   valuesByDate[year] = JSON.parse(JSON.stringify(yearSlice))

   tickerFunction(yearSlice)

   year++;
   if (year > yearEnd) ticker.stop();
}, delayDuration);


document.querySelector("#select-year").innerHTML = createSelectYear(yearStart)
document.querySelector("#progress-bar-client").innerHTML = createProgressBarByDate(yearStart)

//Select year 
document.querySelector("#select-year").innerHTML = createSelectYear(year)
document.querySelector("#select-year-select").addEventListener('click', function (event) {
   if (valuesByDate[event.target.value]) {
      year = event.target.value
      tickerFunction(valuesByDate[event.target.value])
   }
});


